# -*- encoding: utf-8 -*-
import argparse
import math
import re
import sys
from collections import Counter
from operator import itemgetter

from . import stopwords


class Ranker:
    """
    Class encapsulating the ranking system given a mock database.

    The class method `proximity` corresponds to the function from
    the task descrition.

    So far the proximity measure is it’s just the Jaccard distance, which
    doesn’t take the term frequency into account. Vectors compared by cosine
    similarity/distance might be better.

    TODO:
        - Add stemming
        - Try tf/idf
        - Boost beginning and perhaps end
        - Spelling correction
        - Support of other languages than English
        - Semantic disambiguation (“continental philosophy”)
        - Use Whoosh or Elasticsearch instead
    """

    _token_pattern = re.compile(r"(\w+(?:['’-]\w+)?)", re.U)

    def __init__(self, database):
        self.database = database

    @classmethod
    def _tokenized(cls, string):
        """Tokenize the string and remove stopwords."""
        tokens = cls._token_pattern.findall(string.lower())
        return [token for token in tokens if token not in stopwords.ENGLISH]

    def _ngramized(tokens, max_length):
        """Generate ngrams of lengths 1 to `max_length` of the token list."""
        assert max_length > 0, '`max_length` needs to be > 0'
        ngrams = []
        for length in range(1, max_length + 1):
            # Creates length = n version of the token list, each moved by one token
            # against the previous one. Then uses zip to transpose the n lists into
            # many lists of n tokens each, the ngrams.
            # Based on https://stackoverflow.com/a/30609050/678861
            ngrams.extend(zip(*[tokens[i:] for i in range(length)]))
        return ngrams

    @classmethod
    def proximity(cls, text, query):
        """Return a proximity measure between text and query."""
        raise NotImplemented

    def __call__(self, query):
        """Return texts ranked by their similarity to the query, similar to dissimilar."""
        ranking = [(text, self.proximity(text, query)) for text in self.database]
        return sorted(ranking, key=itemgetter(1), reverse=True)


class JaccardRanker(Ranker):
    """
    Class encapsulating the ranking system given a mock database.

    The class method `proximity` corresponds to the function from
    the task descrition.

    Here, the proximity measure is it’s just the Jaccard distance, which
    doesn’t take the term frequency into account.
    """

    @staticmethod
    def _jaccard_index(set0, set1):
        """Return the Jaccard index/similarity between two sets."""
        if set0 == set1:  # Avoid division by zero for empty sets
            return 1
        return len(set0 & set1) / len(set0 | set1)

    @classmethod
    def jaccard_index(cls, text, query):
        """Return Jaccard index/similarity between text and query."""
        text_tokens = cls._tokenized(text)
        text_ngrams = cls._ngramized(text_tokens, 3)
        query_tokens = cls._tokenized(query)
        query_ngrams = cls._ngramized(query_tokens, 3)
        return cls._jaccard_index(set(text_ngrams), set(query_ngrams))

    @classmethod
    def proximity(cls, text, query):
        """Return a proximity measure between text and query."""
        return cls.jaccard_index(text, query)


class CosineRanker(Ranker):
    """
    Class encapsulating the ranking system given a mock database.

    The class method `proximity` corresponds to the function from
    the task descrition.

    Here, the proximity measure is the cosine similarity.
    """

    @staticmethod
    def _cosine_similarity(vector0, vector1):
        """
        Return cosine similarity.

        The “vectors” are unsorted, so closer to multisets, but the name vector
        is more intuitive here. Based on https://stackoverflow.com/a/15174569/678861
        """
        intersection = set(vector0.keys()) & set(vector1.keys())
        numerator = sum([vector0[x] * vector1[x] for x in intersection])
        sum0 = sum([vector0[x]**2 for x in vector0.keys()])
        sum1 = sum([vector1[x]**2 for x in vector1.keys()])
        denominator = math.sqrt(sum0) * math.sqrt(sum1)
        if vector0 == vector1:
            return 1.0
        elif not denominator:
            return 0.0
        else:
            return numerator / denominator

    @classmethod
    def cosine_similarity(cls, text, query):
        text_tokens = cls._tokenized(text)
        text_ngrams = cls._ngramized(text_tokens, 3)
        query_tokens = cls._tokenized(query)
        query_ngrams = cls._ngramized(query_tokens, 3)
        return cls._cosine_similarity(Counter(text_ngrams), Counter(query_ngrams))

    @classmethod
    def proximity(cls, text, query):
        """Return a proximity measure between text and query."""
        return cls.cosine_similarity(text, query)


def compare():
    """
    Output to stdout the similarity between stdin and a query string.

    TODO: Create similar entry point for rank.
    """
    parser = argparse.ArgumentParser(
        description='Returns the distance measure between query and text (stdin).')
    parser.add_argument('query', help='Query to compare input to')
    args = parser.parse_args()
    text = sys.stdin.read()
    print('Similarity:', '{:.2}'.format(CosineRanker.proximity(text, args.query)))
