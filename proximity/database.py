# -*- encoding: utf-8 -*-
"""A mocked representation of a database containing documents."""


DATABASE = [
    ('Continental does not have an office in Zürich.'),
    ('The Burke County location of Continental is a division of a larger '
     'corporation with sales of around $46 billion in 2013. Continental is among '
     'the leading automotive suppliers worldwide. As a supplier of brake '
     'systems, systems and components for powertrains and chassis, '
     'instrumentation, infotainment solutions, vehicle electronics, tires, and '
     'technical elastomers, Continental contributes to enhanced driving safety '
     'and global climate protection.  Continental is also an expert partner in '
     'networked automobile communication.  Continental currently employs around '
     '178,000 people in 49 countries. The two Morganton facilities are part of '
     'the Vehicle Dynamics Business Unit, which is one of four Business Units '
     'within Continental.'),
    ('“We are proud of our company, our products and our facilities,” said Naomi '
     'Cole, senior human resource manager for Continental Automotive Systems in '
     'Morganton. “Each of us has an important job to do, and ultimately the '
     'success of our company depends upon how well we individually and '
     'collectively perform our jobs and ultimately satisfy our customers. We '
     'seek employees who have the ability to contribute toward our success in a '
     'meaningful way. It is the success of our company as a profitable business '
     'that will increase our job security, opportunities for personal growth, '
     'and support Burke and the surrounding counties.”'),
    ('The report reveals that Bridgestone, Michelin, Goodyear, Pirelli and '
     'Continental are few of the dominant tyre manufacturers in the UAE, '
     'accounting for a substantial share in the country\'s tyre market. These '
     'leading players are constantly growing due to their well­established '
     'supply chain network, comprising exclusive distributorships and local '
     'dealers.'),
    ('The core idea of effective altruism: Effective altruism is using evidence '
     'and analysis to take actions that help others as much as possible. The world '
     'has a lot of horrible problems but we can’t work on them all at once. Our goal '
     'is to accomplish the most good for the world with our limited time, energy, '
     'and resources. There is a lot of agreement among people in the movement about '
     'what questions we should ask when figuring out what will help others the most. '
     'These include: (1) How many does this action affect, and by how much? (Because '
     'we want to improve lives by as much as possible.) (2) Is this the very best '
     'thing you can do? (Because different actions often vary hugely in effectiveness, '
     'making the best things much better than average.) (3) What difference does my '
     'action make? (What’s the difference between what happens if I act, and what '
     'would have happened anyway?) (4) What’s the difference I make on the margin? '
     '(A project might be very good, but lack opportunities for extra people to '
     'improve it further.) (5) What’s the probability of success/failure and how '
     'good/bad would that be? As a rule of thumb, effective altruists are looking '
     'for causes or approaches that score well on 3 criteria: (1) Important: helps '
     'many people a lot. (2) Neglected: other people aren’t already taking all of '
     'the good opportunities. (3) Tractable: there’s something practical you can '
     'do that might succeed.'),
    ('Soylent is quite cost-effective.')
]
