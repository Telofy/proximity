# -*- encoding: utf-8 -*-
"""Test the performance of the algorithm."""
from textwrap import shorten

import pytest

from ..database import DATABASE
from ..rank import CosineRanker

# The sample texts were all somewhat relevant to Continental, so I added some
# that are not.
LEAST_SIMILAR = [(0, 4), (1, 4), (2, 4), (3, 4), (0, 5), (1, 5), (2, 5), (3, 5)]


ranker = CosineRanker(DATABASE)


def assert_ordering(ranks, order):
    ranked_texts = [rank[0] for rank in ranks]
    higher_index, lower_index = order
    higher, lower = DATABASE[higher_index], DATABASE[lower_index]
    higher, lower = ranks[ranked_texts.index(higher)], ranks[ranked_texts.index(lower)]
    assert ranks.index(higher) < ranks.index(lower), \
        '“{}” ({:.2}) not before “{}” ({:.2})'.format(
            shorten(higher[0], width=40, placeholder='…'), higher[1],
            shorten(lower[0], width=40, placeholder='…'), lower[1])


@pytest.mark.parametrize('ranks', [ranker('Continental')])
@pytest.mark.parametrize('order', [(0, 2), (0, 3), (1, 2), (1, 3), (2, 3)] + LEAST_SIMILAR)
@pytest.mark.performance
def test_continental(ranks, order):
    """
    Compare the generated proximity ranking to the (diminutive) gold corpus.

    The short one is only about Continental, so Continental is very relevant to it,
    but the second one has lots of Continental, so it’s hard to compare intuitively.
    The last two are easy to compare for humans but probably hard for the machine
    because the penultimate one is only about Continental (highly relevant), while
    the last one is also about Bridgestone, Michelin, Goodyear, and Pirelli.

    :param query: The query string
    :param orders: List of tuples with “database” indices indicating a partial order.
    """
    assert_ordering(ranks, order)


@pytest.mark.parametrize('ranks', [ranker('Continental Breakfast in Zürich')])
@pytest.mark.parametrize('order', [(1, 3), (2, 3)] + LEAST_SIMILAR)
@pytest.mark.performance
def test_continental_breakfast(ranks, order):
    """
    Compare the generated proximity ranking to the (diminutive) gold corpus.

    The system will probably wash up the first entry to the top,
    but that’s just because I won’t be able to take the negation into account.
    There are libraries for that, but implementing one myself would probably go
    beyond the scope of this task. Only the last one seems again less relevant
    than two others.

    :param query: The query string
    :param orders: List of tuples with “database” indices indicating a partial order.
    """
    assert_ordering(ranks, order)


@pytest.mark.parametrize('ranks', [ranker('Continental Automotive Systems, Inc. in Morgaton, NC')])
@pytest.mark.parametrize('order', [(2, 0), (2, 1), (2, 3), (1, 0), (1, 3)] + LEAST_SIMILAR)
@pytest.mark.performance
def test_continental_automotive(ranks, order):
    """
    Compare the generated proximity ranking to the (diminutive) gold corpus.

    A good part of the query features consecutively in the third entry, which
    which should push it ahead of all others. The name of the town also comes up
    in the second one but is misspelled in the query.

    :param query: The query string
    :param orders: List of tuples with “database” indices indicating a partial order.
    """
    assert_ordering(ranks, order)


@pytest.mark.parametrize('ranks', [ranker('This is not a system')])
@pytest.mark.parametrize('order', [])
@pytest.mark.performance
def test_system(ranks, order):
    """
    Compare the generated proximity ranking to the (diminutive) gold corpus.

    Query has nothing to do with any of the entries, so no preference. The two
    entries that contain “systems” will probably score higher, but that seems
    like noise to me.

    :param query: The query string
    :param orders: List of tuples with “database” indices indicating a partial order.
    """
    assert_ordering(ranks, order)
