# -*- encoding: utf-8 -*-
"""Test the correctness of the implementation."""
from collections import Counter

import pytest

from ..database import DATABASE
from ..rank import CosineRanker, JaccardRanker

jaccard_ranker = JaccardRanker(DATABASE)
cosine_ranker = CosineRanker(DATABASE)


@pytest.mark.parametrize('string, tokens', [
    ('This is not a system', ['system']),
    ('This sentence, which contains punctuation, ends with a bang!',
     ['sentence', 'contains', 'punctuation', 'ends', 'bang']),
    ('The Supreme Mugwump.', ['supreme', 'mugwump']),
    ('', []),
])
def test_tokenized(string, tokens):
    assert CosineRanker._tokenized(string) == tokens


@pytest.mark.parametrize('tokens, ngrams', [
    (['system'], [('system',)]),
    (['sentence', 'contains', 'punctuation', 'ends', 'bang'], [
        ('sentence',), ('contains',), ('punctuation',), ('ends',), ('bang',),
        ('sentence', 'contains'), ('contains', 'punctuation'), ('punctuation', 'ends'),
        ('ends', 'bang'), ('sentence', 'contains', 'punctuation'),
        ('contains', 'punctuation', 'ends'), ('punctuation', 'ends', 'bang')]),
    (['supreme', 'mugwump'], [('supreme',), ('mugwump',), ('supreme', 'mugwump')]),
    ([], []),
])
def test_ngramized(tokens, ngrams):
    assert CosineRanker._ngramized(tokens, 3) == ngrams


@pytest.mark.parametrize('set0, set1, similarity', [
    ({'system'}, {''}, 0.0),
    ({'sentence', 'contains', 'punctuation', 'bang'}, {'sentence', 'contains', 'bang'}, 0.75),
    ({'supreme', 'mugwump'}, {'mugwump'}, 0.5),
    ({}, {}, 1.0),
])
def test_jaccard_index_private(set0, set1, similarity):
    assert JaccardRanker._jaccard_index(set0, set1) == similarity


@pytest.mark.parametrize('text, query, similarity', [
    ('system', '', pytest.approx(0.0, 0.1)),
    ('This sentence contains punctuation and bang!', 'This sentence contains a bang!',
     pytest.approx(0.36, 0.1)),
    ('The Supreme Mugwump', 'A mugwump', pytest.approx(0.33, 0.1)),
    ('', '', pytest.approx(1.0, 0.1)),
])
def test_jaccard_index(text, query, similarity):
    assert JaccardRanker.jaccard_index(text, query) == similarity


@pytest.mark.parametrize('set0, set1, similarity', [
    (Counter({'system'}), Counter({''}), 0.0),
    (Counter({'sentence', 'contains', 'punctuation', 'bang'}),
     Counter({'sentence', 'contains', 'bang'}), pytest.approx(0.87, 0.1)),
    (Counter({'supreme', 'mugwump'}), Counter({'mugwump'}), pytest.approx(0.70, 0.1)),
    (Counter({}), Counter({}), 1.0),
])
def test_cosine_similarity_private(set0, set1, similarity):
    assert CosineRanker._cosine_similarity(set0, set1) == similarity


@pytest.mark.parametrize('text, query, similarity', [
    ('system', '', pytest.approx(0.0, 0.1)),
    ('This sentence contains punctuation and bang!', 'This sentence contains a bang!',
     pytest.approx(0.54, 0.1)),
    ('The Supreme Mugwump', 'A mugwump', pytest.approx(0.58, 0.1)),
    ('', '', pytest.approx(1.0, 0.1)),
])
def test_cosine_similarity(text, query, similarity):
    assert CosineRanker.cosine_similarity(text, query) == similarity
    assert CosineRanker.proximity(text, query) == similarity


@pytest.mark.parametrize('query', [
    'There is no spork.',
    'Reducing extreme, involuntary suffering.',
    'Making a show of scorning continental philosophy to signal erudition.'
    '',
])
def test_call(query):
    rank = cosine_ranker(query)
    assert isinstance(rank, list)
    assert len(rank) == len(DATABASE)
