Proximity
=========

A quick proof of concept of a simple proximity/distance/relevance score.

Install
-------

::

    make install

Tests
-----

Unit tests::

    make test

Tests for the performance of the algorithm::

    make test-performance

- The Jaccard proximity measure fails for 7 of my 36 gold corpus entries.
- The cosine similarity measure fails for 3 of the 36 entries.

CLI
---

You can test the algorithm on the command line like so::

    echo 'Simple sample' | bin/compare 'This is a simple test.'

Todo
----

- Add stemming
- Use tf/idf
- Boost beginning and perhaps end
- Try to correct spelling mistakes
- Support of other languages than English
- Semantic disambiguation (“continental philosophy”)
- Use Whoosh or Elasticsearch instead
