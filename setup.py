#!/usr/bin/env python
from setuptools import setup

setup(
    name='proximity',
    version='0.0.1',
    author='Denis Drescher',
    author_email='denis.drescher@claviger.net',
    include_package_data=True,
    install_requires=[],
    zip_safe=False,
    entry_points={
        'console_scripts': [
            'compare = proximity.rank:compare',
        ]
    }
)
