SHELL = /bin/bash
PYTHON = $(shell which python3.7 || which python3.6 || which python3.5 || which python3.4)

default: install

bin/pip:
	${PYTHON} -m venv .

bin/py.test: bin/pip
	bin/pip install pytest

install: bin/pip
	bin/pip install pip==9.0.1 wheel==0.29.0 six==1.10.0 setuptools==35.0.2
	bin/pip install -c versions.txt -e .

test: bin/py.test
	bin/py.test -m 'not performance' proximity

test-performance: bin/py.test
	bin/py.test -m 'performance' proximity

clean:
	rm -Rf bin include lib local
